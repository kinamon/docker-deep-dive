
---

# Docker Compose

---
# Docker Compose

Compose is a tool for defining and running multi-container Docker applications. With Compose, you use a YAML file to configure your application’s services. Then, with a single command, you create and start all the services from your configuration

Using Compose is basically a three-step process:

- Define your app’s environment with a Dockerfile so it can be reproduced anywhere.
- Define the services that make up your app in docker-compose.yml so they can be run together in an isolated environment.
- Run **docker compose up** and the Docker compose command starts and runs your entire app. You can alternatively run docker-compose up using the docker-compose binary.

---

# Docker Compose (cont.)


Compose has commands for managing the whole lifecycle of your application:

- Start, stop, and rebuild services
- View the status of running services
- Stream the log output of running services
- Run a one-off command on a service


---

# Installing Docker Compose


Docker Compose relies on *Docker Engine* for any meaningful work, so make sure you have *Docker Engine* installed either locally or remote, depending on your setup.

- On desktop systems like Docker Desktop for Mac and Windows, Docker Compose is included as part of those desktop installs.
- On Linux systems, first install the Docker Engine for your OS as described earlier in the course, then run the next command to install *docker-compose*


```sh
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```

the last step is optional, in case you don't have */usr/local/bin* in your *$PATH* variable.

---

# Practice

- Install docker compose from cli using latest repository at github

---
# Compose Commands

here is a short list of mainly used commands with *docker-compose*

- build: helps to build the image
- up: creates and starts the containers
- down: stop and remove containers, networks, images and volumes
- ps: list containers
- stop: stop the service
- start: starts the service
- restart: restarts the service

---

# Creating a Compose File

typically, docker compose file is called **docker-compose.yaml**. one can give a different name , but in that case you'll have use *-f* flag to provide the path for the file to work.

compose files are usually yaml files, although there is an option of writing and using it in json.

the classic docker-compose file usually consists of 4 top level keys:

- version: a mandatory key, that defines to which api of docker you need to communicate with.
- services: defines the list of containers that we're going to use. name of the service will the name of the actual container
    - it is possible to do image builds with docker compose  by using *build* key  and pass to it your *context* of actual dockerfile and *args* for build
- volumes
- networks

---

# Using Volumes And Networking With Compose

