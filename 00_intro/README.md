# Docker Deep Dive



.footer: Created By Alex M. Schapelle, VAioLabs.io


---

# About The Course Itself ?

We'll learn several topics mainly focused on:

- What is docker ?
- Who needs docker ?
- How docker works ?
- How to manage docker in various


### Who Is This course for ?

- Junior/senior sysadmins who have no knowledge of containers
- for junior/senior developers who are still developing on LAMP/LEMP/WAMP/MAMP stacks
- Experienced ops who need refresher


---

# Course Topics

- Intro
- Docker internals
- Docker basics
- Networking
- Storage
- Docker images
  - Dockerfile
  - Building and distributing images
  - Managing images

---
# Course Topics (cont.)

- Beyond basics
  - Container Management
  - Docker Compose
  - Docker container orchestration platform: K8s

---
# About Me
<img src="99_misc/.img/me.jpg" alt="drawing" style="float:right;width:180px;">

- Over 12 years of IT industry Experience.
- Fell in love with AS-400 unix system at IDF.
- 5 times tried to finish degree in computer science field
    - Between each semester, I tried to take IT course at various places.
        - Yes, one of them was A+.
        - Yes, one of them was cisco.
        - Yes, one of them was RedHat course.
        - Yes, one of them was LPIC1 and Shell scripting.
        - No, others i learned alone.
        - No, not maintaining debian packages any more.
---

# About Me (cont.)
- Over 7 years of sysadmin:
    - Shell scripting fanatic
    - Python developer
    - Js admirer
    - Golang fallen
    - Rust fan
- 5 years of working with devops
    - Git supporter
    - Vagrant enthusiast
    - Ansible consultant
    - Container believer
    - K8s user

You can find me on linked-in: [Alex M. Schapelle](https://www.linkedin.com/in/alex-schapelle)

---
# History

## Software Development Environment Dilemma: where to develop ?

- Local Development
- Development At Virtual Machines
- Containers: LXc, Docker, Podman and etc.


---
# Local Development
<!-- mention stackoverflow survey -->
<img src="99_misc/.img/survey.png" alt="drawing" style="float:right;width:400px;">

Installing all in your working laptop/desktop to achieve **Dev-Prod Parity**

Here is list of problems with that:

UNIX/Linux and Windows differences

- File encoding
- Case sensitive
- Permission
- In windows, things may not work, as example, windows XP to 7 upgrade
- Once the project gets bigger, working on your direct box might become inconvenient

How to fix it all ? Well, next natural step is to move on to **Virtual Environments**

---
# Development At Virtual Machines

By running with virtualized environment with tools like **VirtualBox, VMWare, KVM or Parrallel**. 
one can even argue, that it can be easily automated with tools such as Vagrant,
to which you can save code separately.

All seems fine until others will join your work and then these start to happen:

- Configuration drift, meaning that current config will be different from what you started with.
- Mutable infrastructure, suggesting that your infrastructure might change while running in production and that might have dangerous effect applications running it them.
  
---

# Development At Virtual Machines (cont.)

- Maintains updates of VM is also an issue that requires attention of specialized team which in cases of start-ups is not an case.
- It all can be automated with software such as **vagrant**, adding another layer of complexity does not always solves your initial problem and also might become bigger issue in future.

Working on laptops, has additional effect on virtual environments. my laptop with 8 cores of cpu and 32 gb ram,
with suitable storage will run the virtual environment differently from some elses laptop who has only 2 cores of cpu 8gb ram.

So there is no more use cases for VMs? well not entirely. VMs are still part of our life and they will continue to be so for long but use cases are shifting from what they used to be.

In order to involve from this place, solution was provided in shape of **container**, which is next natural step.

---
# Containers

__Definition__: a **container** is an object for holding or transporting something.

This is also the use case with software development. containers provide:

- **Immutability** that was an issue with VM.
- They only contain libraries and binaries of required software.
- Code or external things that might need change, are inserted from external sources.

Why is it so important ?
By separating data, libraries, binaries and files, we are creating immutable services. if changes need to happen, we take down the container and bring up the new one, thus **configuration drift** can never happen.

---

# Containers (cont.)

Same goes for **Dev-Prod parity**, because containers can not change, what ever works in **Dev**, same will work in **Prod**. 

What about updates and patches ? well the thing that is updated is images, but from container point of view, they are read-only filesystems, but those do not have effect on main process of binary running inside of container.

Main binary in side of container ? yes, container only run what is required and nothing else.

---

# What Is Docker ?
<!-- Docker is a set of platform as a service (PaaS) products that use OS-level virtualization to deliver software in packages called containers. Containers are isolated from one another and bundle their own software, libraries and configuration files; they can communicate with each other through well-defined channels. Because all of the containers share the services of a single operating system kernel, they use fewer resources than virtual machines -->
1. Company:

- Docker inc.
  - Based in San Francisco
  - Founded by Solomon Hykes
  - Started as PaaS provider named dotCloud
  - Leveraged LXC
  - Their internal tool used to manage containers named docker
  - In 2013 they re-branded to Docker

---

# What Is Docker ? (cont.)

2. Container runtime and orchestration engine

- Most people are referring to the docker engine:
  - 2 main editions:
   - Enterprise Edition (EE)
   - Community Edition (CE)
  - Both are released quarterly:
   - EE: supported for 12 month
   - CE: supported for 4 month

---
# What Is Docker ? (cont.)

3. Docker open-source project

- Also called Moby
  - Upstream project of docker
  - Breaks docker down into more modular components


<!-- 
---

# Why Not Vm's Or Bare Metal?

<img src="./vm.png" alt="drawing" style="float:right;width:400px;">

A BareMetal or VM are essentially working in the same manner: they keep an OS and on it, they keep run enviroments, working libraries ,daemons/services and our predefined code that is used as Frontend/Backend application. Early example could be called: LAMP, LEMP,MAMP, WAMP and so on.

Incase of VM's, they can also be described as an emulation of a real computer that executes programs like a real computer. VMs run on top of a physical machine using a `hypervisor`. A hypervisor, in turn, runs on either a host machine or on BareMetal.

It all seems nice and easy until one reaches the bottle neck of BareMetal, in which case  -->

<!-- ---

# Why To Use Docker ?

<img src="./container.png" alt="drawing" style="float:right;width:400px;">


- dev/prod parity
- simplifying configuration
- code pipeline management
- developer productivity
- application isolation
- server consolidation
- debugging capabilities
- multi-tenancy -->
