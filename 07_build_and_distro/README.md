
---
# Building and Distribution Images

---

# Building Images

As mentioned the build can be done with

```sh
docker image build -t my-img:v1 .
```

But there are additional run the same command

```sh
docker image build -t my-img:v1 - < Dockerfile
```


---
# Building Images (cont.)


There are some flags that can be very useful while building the images and those are:

- *-f* or *--file* : name of the Dockerfile
- *--force-rm*: always remove intermediate containers
- *--label*: set meta-data for an image
- *--rm* removes intermediate containers after a successful build
- *--ulimit*: user space limits for containers

An example:

```sh
docker image build -t vaiolabs/test:build-test --label io.vaiolabs.version=v0.9 -f Dockerfile.current .
```

*Dockerfile.current* is just a specific docker file.(in case you'd like to have several docker files on the project)

---
# Building Images (cont.)


There is also an option of piping the docker file content through *STDIN* e.g.

```sh
docker image build -t vaiolabs/nginx:stdin --rm - << EOF
FROM nginx:latest
VOLUME ["/usr/share/nginx/html"]
EOF
```

---

# Building Multi-Stage Builds

- By the default the stages are not named
- Stages are integer numbers
- Starting with 0 for the first FROM instruction
- Name the stage by adding NAME to the FROM instruction
- Reference the stage name in the COPY instruction

---
# Building Multi-Stage Builds (cont.)

```sh
FROM golang:1.16
WORKDIR /go/src/github.com/alexellis/href-counter/
RUN go get -d -v golang.org/x/net/html 
COPY app.go .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

FROM alpine:latest 
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=0 /go/src/github.com/alexellis/href-counter/app .
CMD ["./app"] 
```

<!-- 
---

# Building Multi-Stage Builds (cont.) -->

Which you can build with 

```sh
docker build --target builder -t go/href-counter:latest .
```


---

## Tagging

We already discussed the tagging, but haven't mentioned it in case of *image* command

```sh
docker image tag my-img/my-app:v1 vaiolabs.io/html-app:latest
```

Why to use it if you created label and tag when generating the image ? 

well, its general purpose is used when distributing the docker image to/from dockerhub which is our next topic.

---

# Distribution Images On Docker Hub

In case you have not created account at _[docker hub](https://hub.docker.com)_, then this is the time to do so.

If  you already have the account then, you can issue  a **docker login** command which will allow to connect to your public and private repositories of images.

Once you have created the account and logged in from your terminal, all is left to *push* your custom image to the docker hub or any other hub there is.

That can be done with 

```sh
docker push vaiolabs.io/html-app:latest
```