
---

# Docker Commands

---
# Docker Commands

In this chapter we'll focus on dockers management commands. mainly on initials of docker.

The latest iteration of docker, have had some changes and as such, some of docker
 commands have had changed their command line options. as such we'll start with management commands and their options.

---

# Docker Management Commands

The list of docker management commands is quite long and each command has its own sub-set of additional commands that can be passed. eventually every command and sub-command have additional options which we will discover  during our course. here is list of initial options.

- Builder: manage build
- Config: manage docker configs
- Container:  manage containers
- Engine: manage docker engine
- Image: manage docker images
- Network: manage docker/container networks
<!-- - Node: manage swarm nodes -->

---

# Docker Management Commands (cont.)

- Plugin: manage plugins
- secret: manage docker secrets
- Service: manage services 
- Stack: manage docker stacks
- Swarm: manage swarm itself.
- System: manage docker
- Trust: manage trust on docker images
- Volume: manage volumes
Each and every one of those above have merged into sub commands and have additional
options to run with.

```sh
aschapelle@vaiolabs.io~/$ docker image ls -h 
```

The code above will list help on ls command under docker image

---

# Docker Management Commands (cont.)

Here is short list for several of the commands:

## Docker Image

- ls: list images
- pull: an image or a repository to a registry
- push: an image or a repository to a registry
- inspect: return low-level information on docker objects
- import: the contents from tarball to create filesystem image

---

# Docker Management Commands (cont.)

## Docker Container
  - ls: list container
  - run: run command in a new container
  - inspect: display detailed information on one or more containers
  - top: display detailed information on one or more containers
  - restart: one or more container
  - attach:attach local stdin, stdout and stderr streams to a running container
  - stop: one or more container
  - start: one or more container

---

# Docker Management Commands (cont.)

## Docker Container

  - log: fetch the logs of a container
  - stat: display a live stream of container resource usage
  - exec: run a command in a running container
  - pause: pauses all processes with one or more containers
  - unpause: all processes with one or more containers
  - rm: remove one or more containers
  - export: export a containers filesystem as a tar archive
  - prune: remove all stopped containers

---

# Docker Management Commands (cont.)

## Getting Images

To create container from image, local or remote, we would usually for with **docker container run**. when creating containers it is worth to remember that when ever the container starts it executes its embedded command. once that command finishes, container stops and is terminated.

Here are some examples simple busybox container:

```sh
aschapelle@vaiolabs.io~/$ docker container run busybox
```

There are additional options that can be added to **docker container run**

```sh
aschapelle@vaiolabs.io~/$ docker container run busybox
```

---

# Docker Management Commands (cont.)

## Exposing and Publishing Ports

```sh
docker container run -d --expose 3000 nginx
```

```sh
docker container run -d --expose 3000 -p 8080:80 nginx
```

```sh
docker container run -d --expose 3000 -p 8081:80/tcp -p 8081:80/udp nginx
```


```sh
docker container port silent_mobius # or id of a container
```

---

# Docker Management Commands (cont.)

## Executing Commands in Docker

Essentially every time we run containers, we run commands in them

```sh
docker container run -it  nginx
```

In cases where we would like to add additional commands or by pass internal CMD commands
we could use **exec** command for it.

```sh
docker container exec -it  nginx /bin/bash
```

We can also pass several command via shell. although it needs the verification that correct shell is used.

```sh
docker container exec -it  nginx /bin/bash -c 'apt update ; apt install vim-y'
```

> Note: in future chapters where we'll talk about **dockerfile**s we'll see additional ways to run commands and CMD in docker containers.

---

# Docker Management Commands (cont.)

## Docker Logging

View logs for a container or service

The docker logs command shows information logged by a running container. The docker service logs command shows information logged by all containers participating in a service. The information that is logged and the format of the log depends almost entirely on the container’s endpoint command.

By default, docker logs or docker service logs shows the command’s output just as it would appear if you ran the command interactively in a terminal. UNIX and Linux commands typically open three I/O streams when they run, called STDIN, STDOUT, and STDERR. STDIN is the command’s input stream, which may include input from the keyboard or input from another command. STDOUT is usually a command’s normal output, and STDERR is typically used to output error messages. By default, docker logs shows the command’s STDOUT and STDERR. To read more about I/O and Linux

---

# Docker Management Commands (cont.)

## Docker Logging

Docker includes multiple logging mechanisms to help you get information from running containers and services. These mechanisms are called logging drivers. Each Docker daemon has a default logging driver, which each container uses unless you configure it to use a different logging driver, or “log-driver” for short.

If you do not specify a logging driver, the default is json-file. To find the current default logging driver for the Docker daemon, run docker info and search for Logging Driver.

```sh
docker info --format '{{.LoggingDriver}}'
```

When you start a container, you can configure it to use a different logging driver than the Docker daemon’s default, using the **--log-driver** flag.

---

# Docker Management Commands (cont.)

## Docker Logging

Supported logging drivers:

| Driver 	  | Description |
| ---       | --- |
| none      |	No logs are available for the container and docker logs does not return any output. |
| local     |	Logs are stored in a custom format designed for minimal overhead. |
| json-file |	The logs are formatted as JSON. The default logging driver for Docker.|
| syslog 	  | Writes logging messages to the syslog facility. The syslog daemon must be running on the host machine.|
| journald 	|  Writes log messages to journald. The journald daemon must be running on the host machine. |
| fluentd   |	Writes log messages to fluentd (forward input). The fluentd daemon must be running on the host machine.|


---

## Docker Logging

```sh
docker container logs my-container
```


---

# Practice

- List  all images
- Delete all images if they exist
- Clean all fs/net/volume
- Pull next list of containers:
    - debian
    - rocky
    - nginx
- Run nginx container 
- Validate that container is running