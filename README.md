# Docker Deep Dive

# About The Course Itself ?

We'll learn several topics mainly focused on:

- What is docker ?
- Who needs docker ?
- How docker works ?
- How to manage docker in various


### Who Is This course for ?

- Junior/senior sysadmins who have no knowledge of containers
- for junior/senior developers who are still developing on LAMP/LEMP/WAMP/MAMP stacks
- Experienced ops who need refresher


---

# Course Topics

- Intro
- Docker internals
- Docker basics
- Networking
- Storage
- Docker images
  - Dockerfile
  - Building and distributing images
  - Managing images

---
# Course Topics (cont.)

- Beyond basics
  - Container Management
  - Docker Compose
  - Docker container orchestration platform: K8s


---

> Note: Please use `build.sh` script to create html version for your use