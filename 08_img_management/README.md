
---

# Managing Images

---

## Image History

After images are created, it is requested to look on build history. we can sort internals of the image with *history* command

Usually providing the image name is enough

```sh
docker image history my-image
```

It should provide you with *image ids* and with description of what ran on each of the layers.

In some cases, the whole image id is required and then we can use *--no-trunc* which will print the whole id of the image

In other cases the shorter image id can be achieved with *--quiet* flag


---
# Saving And Loading Images

when ever we are working in closed environment, where there is no access to docker hub, moving and saving docker images with automation or manually.

That is why we have *save* option with **docker image**

To save existing image or image from docker hub

```sh
docker image save my-image > my-saved-image.tar
```

or

```sh
docker image save my-image -o my-saved-image.tar
```

or

```sh
docker image save my-image --output my-saved-image.tar
```

*tar* file will be generated and you'll be able to zip it in any manner you'd wish.

> Note: to see what you have inside tar file use **tar tvf my-saved-image.tar**

---
# Saving And Loading Images (cont.)
 
If you'd like to revert to the use of the image, then *load*  command should be used.

Just as with *save*, locate the tar file and *load* back

```sh
docker image load my-image < my-saved-image.tar
```

or

```sh
docker image save my-image -i my-saved-image.tar
```

or

```sh
docker image save my-image --input my-saved-image.tar
```
