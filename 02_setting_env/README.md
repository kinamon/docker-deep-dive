
---

# Setting Environment

---

## Removing Old Versions

Due to dockers regular updates, it is suggested **NOT** to install docker from your Linux distribution repositories. thus in case you have installed them, here is you can remove them.(in case of RedHat/Centos/Rocky/Fedora)

```sh
bash:~$ sudo yum remove -y docker  docker-client docker-client-latest\
                    docker-common docker-latest docker-latest-logrotate\
                    docker-logrotate docker-engine
```


Or in case you are on **Debian** based OS

```sh
 bash:~$ sudo apt-get remove docker\
             docker-engine docker.io containerd runc
```


---

# Adding Docker Repository

In order to add docker to repository you'll need to access your root user account or on a contrary use `sudo` command. In my case I am going with root user.

We'll start by installing prerequisites

```sh
bash:~$ sudo yum install -y  yum-utils\
             device-mapper-persistent-data lvm2
```
And with **Debian** version

```sh
bash:~$ sudo apt-get update
bash:~$ sudo apt-get install apt-transport-https\
                             ca-certificates curl gnupg lsb-release
```


---
# Adding Docker Repository (cont.)

After adding initial tools, you can use *yum-utils* included to tool *yum-config-manager* to add docker-ce repository as follows:

```sh
bash:~$ sudo yum-config-manager\
        --add-repo https://download.docker.com/linux/centos/docker-ce.repo
```

---
# Adding Docker Repository (cont.)

In case of **Debian** based OS GnuPG certificate is needed, and only then the remote repository will work, so here are list of commands:

```sh
bash:~$  curl -fsSL https://download.docker.com/linux/ubuntu/gpg\
         | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
```


And adding the repo to **sources.list.d** folder

```sh
bash:~$echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg]\
            https://download.docker.com/linux/ubuntu  $(lsb_release -cs)\
            stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```


---

# Adding Docker Repository (cont.)

Adding your repo is not enough, you'll also need to install it, so just run:

```sh
bash:~$ sudo yum install -y docker-ce
```


Same goes for **Debian** based OS

```sh
bash:~$ sudo apt-get update
bash:~$ sudo apt-get install docker-ce docker-ce-cli containerd.io
```


---
# Adding Docker Repository (cont.)

As any service on Linux box, you need to start and enable docker service.

```sh
bash:~$ sudo systemctl enable --now docker
```

In case you are planning not to use **root** user on your box, then it is suggested to add your user to docker group

```sh
bash:~$ sudo usermod -aG docker aschapelle
```
> NOTE: there is no difference between *RedHat/Debian* distro in this step

---
# Adding Docker Repository (cont.)

To verify  that it works it is suggested to test your docker.

```sh
bash:~$ docker version
```
> NOTE: there is no difference between *RedHat/Debian* distro in this step


---
# Adding Docker Repository (cont.)

And if all that is too much for you and if you are as lazy as I am, you can use remote auto-script for install

```sh
bash:~$ wget -O- get.docker.com | sudo bash
```

And test it in the same way.
