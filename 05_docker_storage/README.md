
---

# Docker Storage


---

# Intro To Docker Storage 

- Non-persistent 
    - Data that is ephemeral
    - Every container has it
    - Tied to the lifecycle of the container
- Persistent
    - Volumes
    - Volumes are decoupled from container
---
# Intro To Docker Storage (cont.)

Non-persistent data:

- By default all container use local storage

- Storage locations:
  
    - Linux: /var/lib/docker/<storage-driver>
    - Window: c:\programdata\docker\windowsfilter\

---
# Intro To Docker Storage (cont.)

Storage drivers:

    - RHEL family uses overlay2
    - Ubuntu uses overlay2 or aufs
    - Suse uses btrfs
    - Windows has it's own special case...

---
# Intro To Docker Storage (cont.)

- Volumes:
    - Create the volume
    - Create your container
- Mount to a directory in the container
- Data is written to the volume
- Deleting a container does not delete the volume
- First-class citizens
- Used the local driver as default
- 3rd party drivers
    - Block storage
    - File storage
    - Object storage

In any case of container loss, when the data is written to volume, it will be saved.

---

# Volume Commands

```sh
docker volume -h
```

```sh
docker volume ls
```

```sh
docker volume create my-storage
```

```sh
docker volume inspect my-storage
```


```sh
docker volume rm my-storage
```


```sh
docker volume prune
```

---

# Using Bind Modes

Bind mounts have been around since the early days of Docker. Bind mounts have limited functionality compared to volumes. When you use a bind mount, a file or directory on the host machine is mounted into a container. The file or directory is referenced by its absolute path on the host machine. By contrast, when you use a volume, a new directory is created within Docker’s storage directory on the host machine, and Docker manages that directory’s contents.


```sh
docker container run -d --name my-container --mount type=bind, source="$(pwd)/target,target=/app nginx
```

Bind mounts are not managed by volumes

```sh
docker container run -d --name my-container -v "$(pwd)/target:/app nginx
```

---

# Using Bind Modes (cont.)


Binds can also be used to mount specific file into your container. for example, custome config file for specific file

```sh
docker container run -d --name file-bind-container -v "$(pwd)"/nginx/nginx.conf:/etc/nginx/nginx.conf nginx
```

> note: find the nginx conf file in the misc folder of the project

---

# Using Volumes For Persistent Storage

- Volumes are easier to back-up and/or migrate than bind mounts
- You can manage volumes suing docker cli commands or docker api
- Volumes work on linux and windows container
- Volumes can be more savely share amoung multiple containers
  
- Create a new volume for nginx container
    - ` docker volume create html-volume `
- Mount the volume with mount flag
    - ` docker container run -d --name html-container --mount type=volume,source="$(pwd)"/my-vol,target=/usr/share/nginx/html nginx `

---
# Using Volumes For Persistent Storage (cont.)


  - Same can be done with -v options
      - `docker container run -d --name html-container --mount source="$(pwd)"/my-vol,target=/usr/share/nginx/html nginx`
  - One last thing is permissions on the storage, meaning we can make volumes **readonly**
      - `docker container run -d --name html-container --mount type=volume,source="$(pwd)"/my-vol,target=/usr/share/nginx/html,readonly nginx`


---

# Practice

- Create volume  net-vol
- Create container that uses volume net-vol
- Access container save "foobar" file on the mounted volume
- Exit the container and kill it 
- Verify that foobar exists
