
---

# Container Management

---

# Inspecting Container Processes

```sh
docker container top my-container
```


```sh
docker container stats my-container
```


---

# Having Containers Start Automatically

```sh
docker container run -d --restart always --name always-restart my-image
```

```sh
docker container run -d --restart unless-stopped --name unless-stopped my-image
```

```sh
docker container run -d  --name regular-container my-image
```

restart docker daemon with *sudo systemctl restart docker*

only *always-restart* and *unless-stopped*

<!-- ---

# Docker Events


---

# Managing Stopped Container


---

# Updating Containers With Watchtower -->
