
---

# Docker Architecture


---
# Docker Architecture

<img src="99_misc/.img/docker-arch.png" alt="drawing" style="float:right;width:400px;">

Docker utilizes some of Linux kernel features, in order to function:

 - CGroups (what you can use)
 - Namespaces (what you have)
 - Union filesystem
 - Kernel capabilities (what you are allowed to do by kernel )

---

# Docker Architecture: CGroups (cont.) 

CGroups is Linux kernel feature that limits, accounts for, and isolates resources of collection processes
this allow management of resource per container:

- Memory
- Cpu
- Network
- Disk bandwidth

---

# Docker Architecture: CGroups (cont.) 
 
- Resource limitation: groups can be set to not exceed a configured  memory limit, which also includes the file system cache.
- Prioritization: some groups may get larger share of CPU scheduling or disk I/O throughput
- Accounting: measures how much resources certain systems use, which may be used, for example, billing purposes
- Control: freezing the groups of processes, their checkpoints and restarts

---

# Docker Architecture: Namespaces (cont.) 

Namespaces are layer of isolation: each aspect of container runs in its own namespace and does not have access outside it

- The **`pid`** namespace: used for process isolation
- The **`net`** namespace: used for managing network interfaces
- The **`ipc`** namespace: used for managing inter-process communication
- The **`mnt`** namespace: used for managing mount points
- The **`uts`** namespace: used for managing kernel identifiers (unix timesharing system)
- The **`user`** namespace: used for managing containers from user space.

---

# Docker Architecture: Union File Systems (cont.) 




- Unionfs, or Union filesystem are the are filesystems that operate by creating layers
- Docker uses union filesystems to provide the building blocks for containers
- Docker can make use of several union file system variants include: aufs, btrfs, vfs, devicemapper and many more.
- The most popular is aufs
- Since ubuntu 16.04 the default filesystem on ubuntu is overlayfs

What is it used for, though ? It is the glue that connects container internals in addition of namespaces and cgroups

---

# Docker Architecture (cont.)

- The docker client(docker):
  - Is how users interact with docker
  - the client sends these commands to dockerd
- Docker registry:
  - stores docker images
  - public registry such as dockerhub
  - run own private registry


---

# The Docker Engine

## Modular In Design

  <img src="99_misc/.img/docker-eng.png" alt="drawing" style="float:right;width:500px;">

- Batteries included but replaceable
- Based on open-standards outline by the open container initiative
- The major components:
    - docker client
    - docker daemon
    - containerd
    - runc

---

# The Docker Engine


-  The components work together to create and run containers
-  A brief illustration:
  
  <img src="99_misc/.img/" alt="missing" style="float:right;width:500px;">

---

# The Docker Engine (cont.)

- Initially based on lxc project to use Linux specific tools
 - Namespaces
 - Control groups (cgroups)
 - Scopes

Dockers first release was huge monolithic mess, all in one binary that included:

 - docker client
 - docker daemon
 - docker api
 - container runtime
 - image builds

---

# The Docker Engine (cont.)

- Lib-lxc was later replaced with `libcontainer`
- Docker 0.9 included new version of `libcontainer`
- It also was platform agnostic(working all os types)
- But it was still monolithic:
    - Slow
    - Harder to innovate
    - Not what ecosystem wanted/needed ...
- This opened the door out of box thinking:
    - Smaller and specialized tools
    - Plug-able architecture

---

# The Docker Engine (cont.)

## Open Container Initiative (OCI)

A group that standardized container requirement need to for ecosystem and development.
the idea suggested were:

  - Image spec
  - Container runtime spec
  - Version of standard 1.0 released in 2017
  - Docker inc., heavily contributed to all those specs
  - By the version of 1.11 of docker(2016) most of these specification were mostly implement in docker container environment.

---

# The Docker Engine (cont.)

## Dockers **runc** :

  - Implementation of the OCI container-runtime-spec
  - Lightweight cli wrapper for `libcontainer`
  - Main job: **create containers**
  
---

# The Docker Engine (cont.)

## Dockers **containerd** :

  - Manage containers lifecycle:
      - start
      - stop
      - pause
      - delete
      - image management
  - part of 1.11 docker

---

# The Docker Engine (cont.)

## Dockers **shim** :
  
  - Responsible for :
  - STDIN and STDOUT
  - Reporting exit status to the docker daemon
  - Implementation of daemon-less containers
  - **`Contnerd`** forks and instance of **runc** for each and new container
  - **`Runc`** process exits after container is created
  - Shim process becomes the container parent


---

# The Docker Engine (cont.)

## Running Containers

```sh
aschapelle@vaiolabs.io:~/$ docker container run -it --name <NAME> <IMG>:<TAG>
```


Creating a container:

- Using cli to execute command
- Docker client uses the appropriate api payload
- POSTs to the correct api endpoint
- The docker daemon receives instructions
- The docker daemon calls containerd to start new container
- Docker daemon uses **`grpc`** (CRUD like api)

--- 

# The Docker Engine (cont.)

  <img src="99_misc/.img/docker-daemon-process.png" alt="drawing" style="float:right;width:400px;">

- **`Containerd`** creates an OCI bundle frm docker image
- It tells to **`runc`** to create a container using OCI bundle
- **`Runc`** will interface with os kernel to get the constructs  needed to create a container.  
  - **`Namespaces`**, **`scopes`**, **`cgroups`** and so on...
- Container process is started as a child process
- Once the container starts **`runc`** will exit and **`shim`** will take over the container.
- See below some illustration of sort:


---

# Docker Images and Containers

## What is docker image?

- Essentially, it is a template of union filesystem layers, stacked together, for creating an image.
    - If you are familiar with OOP, one can treat them as classes.
- Containers are instances of **`image`**
- In some sense, images are stopped containers
- Images are comprised of multiple layers
- Images are considered as build time constructs, while containers are considered as run time constructs
  - It worth mentioning that 2 are dependant on each other, meaning that if you wish to remove docker image, you'll have delete all the containers before deleting that image.

---

# Docker Images and Containers (cont.)

<img src="99_misc/.img/docker-layers.png" alt="drawing" style="float:right;width:400px;">

## Images are build from **`dockerfiles`**
  - This file includes instructions on how that container should be set up.
  - The same instructions holds the list of binaries needed to be installed for the application to run, for example for python based web service written with flask, you'll probably use to install python and flask binaries only.
  - Images are made of layers
  - All the layers are readonly, besides the last one, which is only layer that can be changes/edited.
    - If you login to container and install **vim** the binary of it will only be install on the last layer that is editable. it will **NOT** be added to the image or any layer of image.
- Here is an example of image layers

---

# Docker Images and Containers (cont.)


- It is also worth mentioning the ing that different containers can use same image at the same time.
- just look on the illustration:
  
 <img src="99_misc/.img/docker-multi-container-layers.png" alt="drawing" style="float:right;width:400px;">


---

# Docker Hub

## What Is Docker Hub?

- It is public registry: think of it as git but for containers
- Service provided by docker
- Features:
    - Repositories
        - Place to keep your builds and share them with users
    - Teams and organizations
        - Level of managements that you can achieve.

---

# Docker Hub (cont.)

- Official images
    - Base images created by docker to have base images to start from.
- Publisher images
    - List of high quality images provided by 3rd party publishers, for example python container provided by python software foundation.
- Docker hub can build images and push it to your repository
- Web-hooks: to trigger custom changes per your build image.
- We'll be using docker hub, so sign up: `https://hub.docker.com/signup`


---
# Install Lab. 

- Try installing docker on various systems.
  - Linux
  - Windows
  - MacOS
- Test that docker works with serveral commands
  - docker version
  - docker run -it ubuntu /bin/bash