
---
# Docker Networking
---

# Intro To Docker Networking 

Socket networking:

- Container Network Model (CNM)
    - Essentially OSI model for containers
- The *libnetwork* implements CNM
- Driver extend the model by network topologies

---
# Intro To Docker Networking (cont.)


Network drivers:

  - bridge
  - host
  - overlay
  - macvlan
  - none
  - 3rd party network plugins

Used to make containers communicate with each other

---

# Intro To Docker Networking (cont.)

Defines three building blocks:
<img src="99_misc/.img/endpoint.png" alt="drawing" style="float:right;width:400px;">

  - Sandboxes
  - Endpoints
  - Networks

--- 

<!-- ## Docker Networking 101

docker networking:
  - container network model (cnm)
  - the libnetwork implements cnm
  - driver extends the model by network topologies
  
network drivers:
    - bridge
    - host
    - overlay
    - macvlan
    - none
    - 3rd party network plugins from docker hub

---
# Overview (cont.)

defines three build blocks:

-

--- -->

# Networking Commands

List networks

```sh
docker network ls
```

Getting detailed network information

```sh
docker network inspect my-net
```

Create new network

```sh
docker network create my-net
```

Delete  network

```sh
docker network rm my-net
```

---
# Networking Commands (cont.)

Connect a container to a user-defined bridge

```sh
 docker create --name my-nginx --network my-net --publish 8080:80 nginx:latest
```

Disconnect a container from a user-defined bridge

```sh
docker network disconnect my-net my-nginx
```

---
# Networking Commands (cont.)

The overlay network driver creates a distributed network among multiple Docker daemon hosts
This network sits on top of (overlays) the host-specific networks, allowing containers connected to it (including swarm service containers or k8s ) to communicate securely when encryption is enabled. Docker transparently handles routing of each packet to and from the correct Docker daemon host and the correct destination container

To create an overlay network for use with swarm services or k8s , use a command like the following:
```sh
 docker network create -d overlay my-overlay
```

To create an overlay network which can be used by swarm services or standalone containers to communicate with other standalone containers running on other Docker daemons, add the --attachable flag:

```sh
docker network create -d overlay --attachable my-attachable-overlay
```

---


# Networking Containers

Create a network with a subnet and gateway:

```sh
docker network create --subnet 10.100.0.0/16 --gateway 10.100.100.1 br02
```

```sh
docker network create --subnet 10.100.0.0/16 --gateway 10.100.100.1 --ip-range=10.100.4.0/24 br04
```

---

# Practice

- Create network inter-net
- Create container and start it with port 8080:80 opened with host to inter-net
- Create container that is not connected to inter-net
- Connect previous container to inter-net
- Delete containers
- Delete network inter-net